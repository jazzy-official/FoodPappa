import 'package:efood_multivendor_driver/controller/auth_controller.dart';
import 'package:efood_multivendor_driver/controller/order_controller.dart';
import 'package:efood_multivendor_driver/helper/price_converter.dart';
import 'package:efood_multivendor_driver/helper/route_helper.dart';
import 'package:efood_multivendor_driver/util/dimensions.dart';
import 'package:efood_multivendor_driver/util/images.dart';
import 'package:efood_multivendor_driver/util/styles.dart';
import 'package:efood_multivendor_driver/view/base/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class VerifyDeliverySheet extends StatefulWidget {
  final int orderIndex;
  final bool verify;
  final bool cod;
  final double orderAmount;
  VerifyDeliverySheet({@required this.orderIndex, @required this.verify, @required this.orderAmount, @required this.cod});

  @override
  State<VerifyDeliverySheet> createState() => _VerifyDeliverySheetState();
}

class _VerifyDeliverySheetState extends State<VerifyDeliverySheet> {
  XFile _pickedFile;

  XFile get pickedFile => _pickedFile;

  String status = '';

  String getPath='';

  @override
  Widget build(BuildContext context) {
    Get.find<OrderController>().setOtp('');
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).cardColor,
        borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
      ),
      child: GetBuilder<OrderController>(builder: (orderController) {
        return Padding(
          padding: EdgeInsets.all(Dimensions.PADDING_SIZE_LARGE),
          child: Column(mainAxisSize: MainAxisSize.min, children: [

            widget.cod ? Column(children: [
              Image.asset(Images.money, height: 100, width: 100),
              SizedBox(height: Dimensions.PADDING_SIZE_LARGE),

              Text(
                'collect_money_from_customer'.tr, textAlign: TextAlign.center,
                style: robotoMedium.copyWith(fontSize: Dimensions.FONT_SIZE_LARGE),
              ),
              SizedBox(height: Dimensions.PADDING_SIZE_LARGE),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [

                  Text(
                    'Select Delivered Image', textAlign: TextAlign.center,
                    style: robotoMedium.copyWith(fontSize: Dimensions.FONT_SIZE_LARGE),
                  ),
                  OutlineButton(
                    onPressed: chooseImage,
                    child: Text('Choose Image'),
                  ),
                ],
              ),

              SizedBox(height: Dimensions.PADDING_SIZE_LARGE),

              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  '${'order_amount'.tr}:', textAlign: TextAlign.center,
                  style: robotoBold.copyWith(fontSize: Dimensions.FONT_SIZE_LARGE),
                ),
                SizedBox(width: Dimensions.PADDING_SIZE_EXTRA_SMALL),
                Text(
                  PriceConverter.convertPrice(widget.orderAmount), textAlign: TextAlign.center,
                  style: robotoBold.copyWith(fontSize: Dimensions.FONT_SIZE_LARGE, color: Theme.of(context).primaryColor),
                ),
              ]),
              SizedBox(height: widget.verify ? 20 : 40),
            ]) : SizedBox(),

            widget.verify ? Column(children: [
              SizedBox(height: Dimensions.PADDING_SIZE_LARGE),
              Text('collect_otp_from_customer'.tr, style: robotoRegular, textAlign: TextAlign.center),
              SizedBox(height: 40),

              PinCodeTextField(
                length: 4,
                appContext: context,
                keyboardType: TextInputType.number,
                animationType: AnimationType.slide,
                pinTheme: PinTheme(
                  shape: PinCodeFieldShape.box,
                  fieldHeight: 60,
                  fieldWidth: 60,
                  borderWidth: 1,
                  borderRadius: BorderRadius.circular(Dimensions.RADIUS_SMALL),
                  selectedColor: Theme.of(context).primaryColor.withOpacity(0.2),
                  selectedFillColor: Colors.white,
                  inactiveFillColor: Theme.of(context).disabledColor.withOpacity(0.2),
                  inactiveColor: Theme.of(context).primaryColor.withOpacity(0.2),
                  activeColor: Theme.of(context).primaryColor.withOpacity(0.4),
                  activeFillColor: Theme.of(context).disabledColor.withOpacity(0.2),
                ),
                animationDuration: Duration(milliseconds: 300),
                backgroundColor: Colors.transparent,
                enableActiveFill: true,
                onChanged: (String text) => orderController.setOtp(text),
                beforeTextPaste: (text) => true,
              ),
              SizedBox(height: 40),
            ]) : SizedBox(),

            (widget.verify && orderController.otp.length != 4) ? SizedBox() : !orderController.isLoading ? CustomButton(
              buttonText: widget.verify ? 'verify'.tr : 'ok'.tr,
              margin: EdgeInsets.only(bottom: Dimensions.PADDING_SIZE_LARGE),
              onPressed: () {

                print('uploaded path');
                print(_pickedFile);

                if(_pickedFile  != null){
                  Get.find<OrderController>().updateOrderStatusDelivered(widget.orderIndex, 'delivered',_pickedFile.path).then((success) {
                    print('success done=====');
                    print(success);

                    if(success) {
                      print('success done=====');
                      Get.find<AuthController>().getProfile();
                      Get.find<OrderController>().getCurrentOrders();
                      Get.offAllNamed(RouteHelper.getInitialRoute());
                      print('success done=====');

                    }
                  });
                } else{
                  print("Upload delivered product image");
                  /* return Fluttertoast.showToast(
                    msg: 'Toast Message',
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.SNACKBAR,
                    backgroundColor: Colors.blueGrey,
                    textColor: Colors.white,
                    fontSize: 16.0,
                  );*/
                }



               /* Get.find<OrderController>().updateOrderStatus(orderIndex, 'delivered').then((success) {







                 *//* if(success) {
                    Get.find<AuthController>().getProfile();
                    Get.find<OrderController>().getCurrentOrders();
                    Get.offAllNamed(RouteHelper.getInitialRoute());
                  }
                  *//*



                });*/
              },
            ) : Center(child: CircularProgressIndicator()),

          ]),
        );
      }),
    );
  }

  chooseImage() async {

    /*await ImagePicker().pickImage(source: ImageSource.camera)
        .then((image) async {
      if(image!=null && await image.exists()) {
        print("image selected");
        setState(() {
          _image = image;
        });
        //other code
      }
      else {
        print("image not selected");
        //other code
      }
    }).catchError((error) {
      print("Error: "+error.toString());
    });*/


    setState(() async {


      _pickedFile = await ImagePicker().pickImage(source: ImageSource.camera);
      // print('order model status image ======== ');
      //print(_pickedFile.path);

      _pickedFile != null ? _pickedFile.path : null;

      /* getPath=_pickedFile.path;
      if(_pickedFile.path != null){

        print('ggggggggg');

      } else{

        print('hhhhhhhhhh');

      }*/


    });
    setStatus('');



  }

  setStatus(String message) {
    setState(() {
      status = message;
    });
  }
}
